# Ingresar una lista de numeros
# Encuentra e imprime todos sus elementos que sean mayores a su vecino izquierdo
a = [int(s) for s in input("Ingresa los numeros seguidos de un espacio: ").split()]

for i in range(1, len(a)):
    if a[i] > a[i - 1]:
        print(a[i], end=' ')