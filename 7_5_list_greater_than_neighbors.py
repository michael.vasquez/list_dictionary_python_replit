# Ingresar lista de numeros
# Imprimir el numero de elemntos que son mayores a sus dos vecinos
# El ultimo elemento no debe considerarse ya que no tiene dos vecinos

a = [int(s) for s in input("Ingresa los numeros seguidos de un espacio: ").split()]
numero = 0
for r in range(1, len(a)-1):
    if a[r-1] > a[r] > a[r+1]:
        numero += 1
print("En la lista hay", numero, "numeros que se repiten")