"""Es posible colocar 8 reinas en un tablero de ajedrez de 8 × 8 para
que no haya dos reinas que se amenacen entre sí. Por lo tanto,
requiere que no haya dos reinas que compartan la misma fila, columna
o diagonal."""
# Ingresar las 8 pares de coordenadas en x,y
# Indicar si hay un par de reinas q violen esta regla, imprimir "SI"

n = 0
coord = {}
mylist = []
corX = []
corY = []
regla = None

while n < 16:
    a = input("Coordenadas: x y:")
    listaB = list(map(int, a.split()))
    mylist.append(listaB[0])
    mylist.append(listaB[1])
    corX.append(mylist[n])
    corY.append(mylist[n + 1])
    n += 2

i_x = 0
for x in corX:
    for i_x in range(0, 8):
        if x == corX[i_x] and corX.index(x) != i_x:
            regla = True

i_y = 0
for y in corY:
    if regla:
        break
    for i_y in range(0, 8):
        if y == corY[i_y] and corY.index(y) != i_y:
            regla = True
            break

n2 = 0
i = 0
for n2 in range(0, 8):
    if regla:
        break
    for i in range(0, 8):
        if abs(corX[n2] - corX[i]) == abs(corY[n2] - corY[i]) and i != n2:
            regla = True
            break
if regla:
    print("Hay un par de reinas q violen la ley: SI")
else:
    print("Hay un par de reinas q violen la ley: NO")
