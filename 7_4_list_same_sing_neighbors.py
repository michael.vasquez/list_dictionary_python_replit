# Ingresar una lista de numeros
# Encontrar e imprimir el primer par de elementos el mismo signo
# Si no existe ese para imprimir cero

lst = [int(s) for sgit in input("Ingresa los numeros seguidos de un espacio:").split()]
for i in range(1, len(lst)):
    if lst[i] * lst[i - 1] > 0:
        print("La primera pareja son: ", str(lst[i - 1]), str(lst[i]))
        break
    elif i == len(lst) - 1:
        print("0")
